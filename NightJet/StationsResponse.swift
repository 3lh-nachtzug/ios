//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import Foundation

struct StationsResponse: Decodable {
    
    let message: [Station]
}
