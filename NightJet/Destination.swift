//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import Foundation

enum Destination {
    
    case from
    case to
}
