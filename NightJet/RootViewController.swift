//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import UIKit

class RootViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    var fromStation: Station?
    var toStation: Station?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchButton.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fromTextField.isEnabled = true
        toTextField.isEnabled = true
        timeTextField.isEnabled = true
        searchButton.isEnabled = !(fromTextField.text ?? "").isEmpty
            && !(toTextField.text ?? "").isEmpty
            && !(timeTextField.text ?? "").isEmpty
    }
    
    @IBAction func searchAction(_ sender: Any) {
        fromTextField.isEnabled = false
        toTextField.isEnabled = false
        timeTextField.isEnabled = false
        searchButton.isEnabled = false
        
        print("search from \(fromTextField.text) to \(toTextField.text) on \(timeTextField.text)")
        performSegue(withIdentifier: "resultSegue", sender: self)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "destinationSegue" {
            let senderTextField = sender as! UITextField
            let destinationViewController = segue.destination as! DestinationViewController
            if senderTextField == fromTextField {
                destinationViewController.destinationType = .from
                destinationViewController.initialValue = fromTextField.text
            } else if senderTextField == toTextField {
                destinationViewController.destinationType = .to
                destinationViewController.initialValue = toTextField.text
            }
        }
    }
    
    // MARK: - TextField delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == fromTextField || textField == toTextField {
            performSegue(withIdentifier: "destinationSegue", sender: textField)
        } else if textField == timeTextField {
            performSegue(withIdentifier: "timeSegue", sender: textField)
        }
        return false
    }
}
