//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import UIKit

class CountryTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCell", for: indexPath) as! CountryTableViewCell

        if indexPath.row == 0 {
            cell.countryImageView?.image = UIImage(named: "city1.jpg")
            cell.titleLabel.text = "Hamburg"
            cell.descriptionLabel.text = "Die große weite Welt und Seeluft schnuppern: Freie und Hansestadt Hamburg."
        } else if indexPath.row == 1 {
            cell.countryImageView?.image = UIImage(named: "city2.jpg")
            cell.titleLabel.text = "München"
            cell.descriptionLabel.text = "München: Zünftig und schick im Süden Deutschlands"
        }
        cell.countryImageView?.contentMode = .scaleToFill
        
        return cell
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
