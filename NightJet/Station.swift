//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import Foundation

struct Station: Decodable {
    
    let name: String
    let eva: String
    let lat: Double?
    let lng: Double?
}
