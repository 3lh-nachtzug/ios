//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import Foundation

class StationsProvider {
    
    let stations: [Station]
    
    init() {
        do {
            let data = try Data(contentsOf: URL(string: "https://nachtzug.herokuapp.com/api/v1/trainite/stationlist")!)
            let decoder = JSONDecoder()
            let stationsResponse = try decoder.decode(StationsResponse.self, from: data)
            stations = stationsResponse.message
        } catch {
            print(error)
            stations = []
        }
    }
    
    func destinations(from station: Station) -> [Station] {
        do {
            let data = try Data(contentsOf: URL(string: "https://nachtzug.herokuapp.com/api/v1/trainite/destinations?bhfDep=\(station.eva)")!)
            let decoder = JSONDecoder()
            let stationsResponse = try decoder.decode(StationsResponse.self, from: data)
            return stationsResponse.message
        } catch {
            print(error)
            return []
        }
    }
}
