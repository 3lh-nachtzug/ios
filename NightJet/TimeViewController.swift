//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import UIKit

class TimeViewController: UIViewController {

    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var button: UIButton!
    
    var formater: DateFormatter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formater = DateFormatter()
        formater.timeStyle = .none
        formater.dateStyle = .short
        formater.locale = Locale.init(identifier: "de_CH")

        picker.addTarget(self, action: #selector(pickerAction(_:)), for: .valueChanged)
        button.layer.cornerRadius = 5.0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let rootViewController = navigationController?.viewControllers[0] as! RootViewController
        rootViewController.timeTextField.text = formater.string(from: picker.date)
        
        super.viewWillDisappear(animated)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        let rootViewController = navigationController?.viewControllers[0] as! RootViewController
        rootViewController.searchAction(self)
    }
    
    @objc
    private func pickerAction(_ sender: UIDatePicker) {
        let rootViewController = navigationController?.viewControllers[0] as! RootViewController
        button.isEnabled = !(rootViewController.fromTextField.text ?? "").isEmpty
            && !(rootViewController.toTextField.text ?? "").isEmpty
            && picker.date.timeIntervalSinceNow > 0.0
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
