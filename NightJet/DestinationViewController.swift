//
// Copyright (C) Schweizerische Bundesbahnen SBB, 2019.
//

import UIKit
import CoreLocation

class DestinationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var destinationType: Destination!
    var initialValue: String?
    
    let stationsProvider = StationsProvider()
    var possibleStations: [Station] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch destinationType! {
        case .from:
            title = "Ab"
            destinationTextField.placeholder = "Ab"
        case .to:
            title = "An"
            destinationTextField.placeholder = "An"
        }
        destinationTextField.text = initialValue
        switch destinationType! {
        case .from:
            if initialValue == "" {
                var stations: [Station] = stationsProvider.stations
                stations.sort { (station1, station2) -> Bool in
                    if let stationLat1 = station1.lat, let stationLng1 = station1.lng, let stationLat2 = station2.lat, let stationLng2 = station2.lng {
                        let coordStation1 = CLLocation(latitude: stationLat1, longitude: stationLng1)
                        let coordStation2 = CLLocation(latitude: stationLat2, longitude: stationLng2)
                        let myPosition = CLLocation(latitude: 47.376350, longitude: 8.537749)
                        return coordStation1.distance(from: myPosition) < coordStation2.distance(from: myPosition)
                    }
                    return false
                }
                possibleStations = stations
            } else {
                possibleStations = stationsProvider.stations.filter { $0.name.contains(initialValue!) }
                possibleStations.sort { (station1, station2) -> Bool in
                    if let stationLat1 = station1.lat, let stationLng1 = station1.lng, let stationLat2 = station2.lat, let stationLng2 = station2.lng {
                        let coordStation1 = CLLocation(latitude: stationLat1, longitude: stationLng1)
                        let coordStation2 = CLLocation(latitude: stationLat2, longitude: stationLng2)
                        let myPosition = CLLocation(latitude: 47.376350, longitude: 8.537749)
                        return coordStation1.distance(from: myPosition) < coordStation2.distance(from: myPosition)
                    }
                    return false
                }
            }
        case .to:
            let rootViewController = navigationController?.viewControllers[0] as! RootViewController
            if let fromStation = rootViewController.fromStation {
                possibleStations = stationsProvider.destinations(from: fromStation)
            } else {
                possibleStations = stationsProvider.stations
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let rootViewController = navigationController?.viewControllers[0] as! RootViewController
        switch destinationType! {
        case .from:
            rootViewController.fromTextField.text = destinationTextField.text
        case .to:
            rootViewController.toTextField.text = destinationTextField.text
        }
        
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        destinationTextField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return possibleStations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "proposalCell", for: indexPath)
        cell.textLabel?.text = possibleStations[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        destinationTextField.text = possibleStations[indexPath.row].name
        navigationController?.popViewController(animated: true)
        let rootViewController = navigationController?.viewControllers[0] as! RootViewController
        switch destinationType! {
        case .from:
            rootViewController.fromStation = possibleStations[indexPath.row]
        case .to:
            rootViewController.toStation = possibleStations[indexPath.row]
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text ?? ""
        let textRange = Range(range, in: text)!
        let updatedText = text.replacingCharacters(in: textRange, with: string)
        
        switch destinationType! {
        case .from:
            possibleStations = stationsProvider.stations.filter { $0.name.lowercased().contains(updatedText.lowercased()) }
            possibleStations.sort { (station1, station2) -> Bool in
                if let stationLat1 = station1.lat, let stationLng1 = station1.lng, let stationLat2 = station2.lat, let stationLng2 = station2.lng {
                    let coordStation1 = CLLocation(latitude: stationLat1, longitude: stationLng1)
                    let coordStation2 = CLLocation(latitude: stationLat2, longitude: stationLng2)
                    let myPosition = CLLocation(latitude: 47.376350, longitude: 8.537749)
                    return coordStation1.distance(from: myPosition) < coordStation2.distance(from: myPosition)
                }
                return false
            }
        case .to:
            let rootViewController = navigationController?.viewControllers[0] as! RootViewController
            if let fromStation = rootViewController.fromStation {
                possibleStations = stationsProvider.destinations(from: fromStation).filter { $0.name.lowercased().contains(updatedText.lowercased()) }
            } else {
                possibleStations = stationsProvider.stations.filter { $0.name.lowercased().contains(updatedText.lowercased()) }
            }
        }
        tableView.reloadData()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}
